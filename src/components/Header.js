import React from 'react';
import Logo from "../assets/img/logo.svg"

const Header = ({ handleToggleDarkMode }) => {
	return (
		<div className='header'>
            <img src={Logo} alt="logo du site "/>
            <h2> Notes </h2>
            <button
				onClick={() =>
					handleToggleDarkMode(
						(previousDarkMode) => !previousDarkMode
					)
				}
				className='save'
			>
				Toggle Mode
			</button>
		</div>
	);
};

export default Header;
